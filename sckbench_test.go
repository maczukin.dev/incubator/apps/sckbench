package sckbench

import (
	"context"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type listener struct {
	inner net.Listener
	addr  string
}

func (l *listener) Close() error {
	if l.inner == nil {
		return nil
	}

	return l.inner.Close()
}

func (l *listener) Addr() string {
	return l.addr
}

func TestSCKBench_Run(t *testing.T) {
	type listenerFactory func(t *testing.T) *listener
	type ctxFactory func(t *testing.T) context.Context
	type dialerFactory func(t *testing.T) *mockConn

	testAddr := ":65000"

	doneCtx := func(_ *testing.T) context.Context {
		ctx, cancelFn := context.WithCancel(context.Background())
		cancelFn()

		return ctx
	}

	staticAddr := func(a string) listenerFactory {
		return func(_ *testing.T) *listener {
			return &listener{addr: a}
		}
	}

	newDialer := func(t *testing.T, cm *mockConn, err error) dialer {
		return func(network, address string) (conn, error) {
			assert.Equal(t, Network, network)
			assert.Equal(t, testAddr, address)

			return cm, err
		}
	}

	tests := map[string]struct {
		addr          listenerFactory
		count         int64
		ctx           ctxFactory
		dialer        dialerFactory
		dialerError   error
		expectedError error
	}{
		"wrong address": {
			addr:          staticAddr(""),
			count:         1,
			ctx:           doneCtx,
			expectedError: ErrCantConnect,
		},
		"error on dialing": {
			addr: func(t *testing.T) *listener {
				return &listener{addr: testAddr}
			},
			count: 1,
			ctx:   doneCtx,
			dialer: func(t *testing.T) *mockConn {
				return newMockConn(t)
			},
			dialerError:   assert.AnError,
			expectedError: ErrCantConnect,
		},
		"closed listener": {
			addr: func(t *testing.T) *listener {
				l, err := net.Listen(Network, "")
				require.NoError(t, err)
				require.NoError(t, l.Close())

				return &listener{addr: l.Addr().String()}
			},
			count:         1,
			ctx:           doneCtx,
			expectedError: ErrCantConnect,
		},
		"error on close": {
			addr: func(t *testing.T) *listener {
				return &listener{addr: testAddr}
			},
			count: 1,
			ctx:   doneCtx,
			dialer: func(t *testing.T) *mockConn {
				d := newMockConn(t)
				d.EXPECT().Close().Return(assert.AnError).Once()

				return d
			},
			expectedError: ErrClosingConnection,
		},
		"valid connection": {
			addr: func(t *testing.T) *listener {
				l, err := net.Listen(Network, "")
				require.NoError(t, err)

				return &listener{addr: l.Addr().String()}
			},
			count: 1,
			ctx:   doneCtx,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			require.NotNil(t, tt.addr, "missing addr value in the test case definition")
			require.NotNil(t, tt.ctx, "missing ctx value in the test case definition")

			l := tt.addr(t)
			defer func() {
				err := l.Close()
				assert.NoError(t, err)
			}()

			b := New(l.Addr(), tt.count)

			if tt.dialer != nil {
				b.dialer = newDialer(t, tt.dialer(t), tt.dialerError)
			}

			err := b.Run(tt.ctx(t))

			if tt.expectedError != nil {
				assert.ErrorIs(t, err, tt.expectedError)
				return
			}

			assert.NoError(t, err)
		})
	}
}

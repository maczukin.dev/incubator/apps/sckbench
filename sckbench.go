package sckbench

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"sync"
)

const (
	Network = "tcp"
)

var (
	ErrCantConnect       = errors.New("can't connect")
	ErrClosingConnection = errors.New("closing connection")
)

//go:generate mockery --inpackage --inpackage-suffix --case=underscore --with-expecter --name=conn
type conn interface {
	io.ReadWriteCloser
}

type dialer func(network, address string) (conn, error)

type SCKBench struct {
	addr  string
	count int64

	dialer dialer

	connectionsLock sync.Mutex
	connections     []conn
}

func New(addr string, count int64) *SCKBench {
	return &SCKBench{
		addr:  addr,
		count: count,
		dialer: func(network, address string) (conn, error) {
			return net.Dial(network, address)
		},
	}
}

func (b *SCKBench) Run(ctx context.Context) (err error) {
	err = b.openConnections(b.count)
	if err != nil {
		return err
	}

	defer func() {
		err = b.closeConnections()
	}()

	<-ctx.Done()

	return err
}

func (b *SCKBench) openConnections(i int64) error {
	for j := int64(0); j < i; j++ {
		err := b.openConnection()
		if err != nil {
			return err
		}
	}

	return nil
}

func (b *SCKBench) openConnection() error {
	fmt.Println("Connecting to", b.addr)

	conn, err := b.dialer(Network, b.addr)
	if err != nil {
		return fmt.Errorf("%w: %v", ErrCantConnect, err)
	}

	b.connectionsLock.Lock()
	defer b.connectionsLock.Unlock()

	fmt.Println("Connected", len(b.connections))
	b.connections = append(b.connections, conn)

	return nil
}

func (b *SCKBench) closeConnections() error {
	var err error
	var errCount int64

	fmt.Println()
	for i, c := range b.connections {
		fmt.Println("Closing connection", i)

		e := c.Close()
		if e != nil {
			err = e
			errCount++
		}
	}

	if err == nil {
		return nil
	}

	return fmt.Errorf("%w: last error: %v; overall closing errors: %d", ErrClosingConnection, err, errCount)
}

package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"time"

	"go.maczukin.dev/apps/sckbench"
)

var (
	help    = flag.Bool("help", false, "Show help information and exit")
	version = flag.Bool("version", false, "Show version and exit")

	addr  = flag.String("address", ":80", "Address that SCKBench should connect to")
	count = flag.Int64("count", 1, "Number of concurrent connections to open")

	timeout = flag.Duration("timeout", time.Duration(0), "Timeout for overall execution; 0 means infinite")
)

func main() {
	flag.Parse()

	if *help {
		flag.Usage()
		os.Exit(1)
	}

	if *version {
		fmt.Println(sckbench.VersionInfo.Full())
		os.Exit(1)
	}

	baseCtx := context.Background()
	ms := timeout.Milliseconds()
	if ms != 0 {
		var timeoutCancel func()

		fmt.Println("Setting automatic timeout for", *timeout)

		baseCtx, timeoutCancel = context.WithTimeout(context.Background(), *timeout)
		defer timeoutCancel()
	}

	ctx, cancel := signal.NotifyContext(baseCtx, os.Interrupt)
	defer cancel()

	b := sckbench.New(*addr, *count)

	err := b.Run(ctx)
	if err != nil {
		fmt.Printf("Failed with: %v\n", err)
		os.Exit(1)
	}
}

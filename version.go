package sckbench

import (
	"fmt"
	"runtime"
)

const (
	Name = "SCKBench"
)

var (
	Version   = "vDEV"
	Revision  = "HEAD"
	Reference = "HEAD"
	Built     = "now"
	GoVersion = runtime.Version()
	OS        = runtime.GOOS
	Arch      = runtime.GOARCH
)

type versionInfo struct {
	Version      string `json:"version"`
	Revision     string `json:"revision"`
	GitReference string `json:"git_reference"`
	BuiltAt      string `json:"built_at"`
	GoVersion    string `json:"go_version"`
	OS           string `json:"os"`
	Arch         string `json:"arch"`
}

func (vi versionInfo) Line() string {
	return fmt.Sprintf("%s %s (%s; %s; %s/%s)", Name, Version, Revision, GoVersion, OS, Arch)
}

func (vi versionInfo) Full() string {
	var s string

	s += fmt.Sprintf("Version:    %s\n", Version)
	s += fmt.Sprintf("Revision:   %s\n", Revision)
	s += fmt.Sprintf("Git ref:    %s\n", Reference)
	s += fmt.Sprintf("Built at:   %s\n", Built)
	s += fmt.Sprintf("GO Version: %s\n", GoVersion)
	s += fmt.Sprintf("OS/Arch:    %s/%s", OS, Arch)

	return s
}

var (
	VersionInfo = versionInfo{
		Version:      Version,
		Revision:     Revision,
		GitReference: Reference,
		BuiltAt:      Built,
		GoVersion:    GoVersion,
		OS:           OS,
		Arch:         Arch,
	}
)

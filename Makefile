.DEFAULT_GOAL := build

NAME := sckbench

export VERSION := v$(shell cat VERSION)
export OUT_PATH ?= build
export CGO_ENABLED ?= 0

REVISION := $(shell git rev-parse --short=8 HEAD || echo unknown)
REFERENCE := $(shell git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1)
BUILT := $(shell date -u +%Y-%m-%dT%H:%M:%S%z)
PKG = $(shell go list .)

OS_ARCHS ?= darwin/amd64 darwin/arm64 \
			linux/amd64 linux/386 linux/arm64 linux/arm

GO_LDFLAGS ?= -X $(PKG).Version=$(VERSION) \
              -X $(PKG).Revision=$(REVISION) \
              -X $(PKG).Reference=$(REFERENCE) \
               -X $(PKG).Built=$(BUILT) \
              -w -extldflags '-static'

.PHONY: .mods
.mods:
	go mod download

.PHONY: build
build:
	@mkdir -p $(OUT_PATH)
	go build -a -ldflags "$(GO_LDFLAGS)" -o $(OUT_PATH)/$(NAME) ./cmd/$(NAME)/

.PHONY: build-os-arch
build-os-arch: GOOS ?= $(shell go env GOOS)
build-os-arch: GOARCH ?= $(shell go env GOARCH)
build-os-arch:
	@mkdir -p $(OUT_PATH)
	go build -a -ldflags "$(GO_LDFLAGS)" -o $(OUT_PATH)/$(NAME)-$(GOOS)-$(GOARCH) ./cmd/$(NAME)/


.PHONY: test
test: .mods
	go test ./...

.PHONY: go-generate
go-generate: mockery
	go generate ./...

.PHONY: mockery
mockery:
	go install github.com/vektra/mockery/v2@v2.27.1
